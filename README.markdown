## GoBlog-api extension


An api extension to expose api to **PyBlog** datastore, and mainly serves as part of my research to see how well 2 different versions of completely different code bases (**Django/Python** and **Go**) serving the same set of datastore.

This proves that with this approach, I can eliminate the usage of `remote_api` and serve for efficient frontend GET requests from a completely different application.


I am convinced **Go** will perform better than **Django/RestFramework**, seeing the low latency with this tiny app and it only uses 1/3rd of memory resource compare to **Django**’s equivalent, which within **GAE** instance limit, is a big deal.

I have just learnt **Go** today and it seemed it has similar syntax as **C**, with much less libraries of course. I will keep learning this great performer language.
