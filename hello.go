package hello

import (
    "net/http"
    "time"
    "appengine"
    "appengine/datastore"
    "encoding/json"
)

type Article struct {
    Author string `json:",omitempty" datastore:"author,noindex"`
    Title string `json:"title" datastore:"title"`
    Content string `json:"content" datastore:"content,noindex"`
    Create_time time.Time `json:"create_time" datastore:"create_time"`
    Update_time time.Time `json:"update)time" datastore:"update_time"`
    Is_published bool `json:"is_published" datastore:"is_published"`
}


func init() {
    http.HandleFunc("/dummy/", dummyHandle)
    http.HandleFunc("/", articleHandle)
}


func articleHandle(w http.ResponseWriter, r *http.Request) {
    c := appengine.NewContext(r)
    q := datastore.NewQuery("Article")
    var articles []Article
    if _, err := q.GetAll(c, &articles); err != nil {
        http.Error(w, err.Error(), http.StatusInternalServerError)
    }

    w.Header().Set("Content-Type", "application/json; charset=UTF-8")
    w.WriteHeader(200)
    json.NewEncoder(w).Encode(articles)
}

func dummyHandle(w http.ResponseWriter, r *http.Request) {
    var dummy interface{}
    json.Unmarshal([]byte(`[
    {"author":"", "title": "dummy 1", "content": "dummy content", "create_time": "2014-09-20T22:15:45.20073Z", "update_time": "2014-09-20T22:15:45.20073Z", "is_published": false},
    {"author":"", "title": "dummy 2", "content": "dummy content 2", "create_time": "2014-09-20T22:15:45.20073Z", "update_time": "2014-09-20T22:15:45.20073Z", "is_published": true}]`), &dummy)

    w.Header().Set("Content-Type", "application/json; charset=UTF-8")
    w.WriteHeader(200)
    json.NewEncoder(w).Encode(dummy)
}
